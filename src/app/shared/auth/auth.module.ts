import { NgModule, ModuleWithProviders } from '@angular/core';
import { AuthService } from './auth.service';
import { AuthGuardService } from './auth-guard.service';
import { ScopeGuardService } from './scope-guard.service';
import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { Http, RequestOptions } from '@angular/http';

@NgModule({
})
export class AuthModule {

    static forRoot (): ModuleWithProviders {
        return {
            ngModule: AuthModule,
            providers: [AuthService,
                AuthGuardService,
                ScopeGuardService,
                {
                    provide: AuthHttp,
                    useFactory: authHttpServiceFactory,
                    deps: [Http, RequestOptions]
                }]
        }
    }
}

export function authHttpServiceFactory (http: Http, options: RequestOptions) {
    return new AuthHttp(new AuthConfig({
        tokenGetter: (() => localStorage.getItem('access_token')),
        globalHeaders: [{ 'Content-Type': 'application/json' }],
    }), http, options);
}
