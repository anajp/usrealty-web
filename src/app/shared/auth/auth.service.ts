import { Injectable } from '@angular/core';
import { AUTH_CONFIG } from './auth0-variables';
import { Router } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import 'rxjs/add/operator/filter';
import * as auth0 from 'auth0-js';

@Injectable()
export class AuthService {
    lastLoggedEmail = '';
    userProfile: any;
    requestedScopes: String = 'openid profile roles permissions';
    refreshSubscription: any;

    // Configure Auth0
    auth0 = new auth0.WebAuth({
        domain: AUTH_CONFIG.domain,
        clientID: AUTH_CONFIG.clientID,
        redirectUri: AUTH_CONFIG.callbackURL,
        audience: AUTH_CONFIG.audience,
        responseType: 'token id_token',
        scope: this.requestedScopes
    });

    // Create a stream of logged in status to communicate throughout app
    loggedIn = false;
    loggedIn$ = new BehaviorSubject<boolean>(this.loggedIn);

    constructor(private router: Router) {
        if (this.isAuthenticated()) {
            this.setLoggedIn(true);
        }
    }

    setLoggedIn (value: boolean) {
        this.loggedIn$.next(value);
        this.loggedIn = value;
    }

    public login (username: string, password: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.auth0.client.login({
                realm: 'Username-Password-Authentication',
                username,
                password,
            }, (err, authResult) => {
                if (err) {
                    console.log(err);
                    alert(`Error: ${err.error_description}. Check the console for further details.`);
                    return reject(err);
                } else if (authResult && authResult.accessToken && authResult.idToken) {
                    // setting access_token here, because of the getProfile, otherwise it'd pick the last logged profile
                    localStorage.setItem('access_token', authResult.accessToken);
                    this.getProfile((error, res) => {
                        this.setSession(authResult);
                        this.router.navigate(['/']);
                    });
                    return resolve(authResult);
                }
            });
        });
    }

    public inactiveLogin (): void {
        this.auth0.authorize();
    }

    public signup (email: string, password: string): void {
        this.auth0.signup({
            connection: 'Username-Password-Authentication',
            email,
            password,
        }, (err, result) => {
            if (err) {
                console.log(err);
                alert(`Error: ${err.description}. Check the console for further details.`);
                return;
            }
            this.login(email, password);
        });
    }

    public getProfile = (cb): void => {
        const accessToken = localStorage.getItem('access_token');
        if (!accessToken) {
            throw new Error('Access token must exist to fetch profile');
        }

        this.auth0.client.userInfo(accessToken, (err, profile) => {
            if (profile) {
                this.lastLoggedEmail = profile.email;
                this.userProfile = profile;
            }
            cb(err, profile);
        });
    }

    public handleAuthentication (): void {
        this.auth0.parseHash(window.location.hash, (err, authResult) => {
            if (authResult && authResult.accessToken && authResult.idToken) {
                window.location.hash = '';
                this.setSession(authResult);
                this.router.navigate(['/']);
            } else if (err) {
                this.router.navigate(['/login']);
                console.log(err);
                alert(`Error: ${err.error}. Check the console for further details.`);
            }
        });
    }

    private setSession (authResult): void {
        // Set the time that the access token will expire at
        const expiresAt = JSON.stringify(
            (authResult.expiresIn * 1000) + new Date().getTime()
        );

        // If there is a value on the `scope` param from the authResult,
        // use it to set scopes in the session for the user. Otherwise
        // use the scopes as requested. If no scopes were requested,
        // set it to nothing
        const scopes = authResult.scope || this.requestedScopes || '';

        localStorage.setItem('access_token', authResult.accessToken);
        localStorage.setItem('id_token', authResult.idToken);
        localStorage.setItem('expires_at', expiresAt);
        localStorage.setItem('scopes', JSON.stringify(scopes));

        this.setLoggedIn(true);
        this.scheduleRenewal();
    }

    public logout (): void {
        // Remove tokens and expiry time from localStorage
        localStorage.removeItem('access_token');
        localStorage.removeItem('id_token');
        localStorage.removeItem('expires_at');
        localStorage.removeItem('scopes');

        this.setLoggedIn(false);
        this.unscheduleRenewal();
        // Go back to the home route
        this.router.navigate(['/login']);
    }

    public isAuthenticated (): boolean {
        // Check whether the current time is past the
        // access token's expiry time
        const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
        return new Date().getTime() < expiresAt;
    }

    public userHasScopes (scopes: Array<string>): boolean {
        const grantedScopes = JSON.parse(localStorage.getItem('scopes'));

        if (!grantedScopes) {
            return false;
        }

        grantedScopes.split(' ');
        return scopes.every(scope => grantedScopes.includes(scope));
    }

    public renewToken () {
        this.auth0.renewAuth({
            audience: AUTH_CONFIG.audience,
            redirectUri: 'http://localhost:3001/silent',
            usePostMessage: true
        }, (err, result) => {
            if (err) {
                console.log(`Could not get a new token using silent authentication (${err.error}).`);
            } else {
                console.log(`Successfully renewed auth!`);
                this.setSession(result);
            }
        });
    }

    public scheduleRenewal () {
        if (!this.isAuthenticated()) {
            return;
        }
        this.unscheduleRenewal();

        const expiresAt = JSON.parse(window.localStorage.getItem('expires_at'));

        const source = Observable.of(expiresAt).flatMap(
            expiresAtFlatted => {

                const now = Date.now();

                // Use the delay in a timer to
                // run the refresh at the proper time
                return Observable.timer(Math.max(1, expiresAtFlatted - now));
            });

        // Once the delay time from above is
        // reached, get a new JWT and schedule
        // additional refreshes
        this.refreshSubscription = source.subscribe(() => {
            this.renewToken();
            this.scheduleRenewal();
        });
    }

    public unscheduleRenewal () {
        if (!this.refreshSubscription) {
            return;
        }
        this.refreshSubscription.unsubscribe();
    }

    public getLastLoggedEmail () {
        return this.lastLoggedEmail;
    }
}
