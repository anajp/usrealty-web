import { Component } from '@angular/core';
import { AuthService } from './shared/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
    action = false;
    interval: any;
    inactiveMinutes = 0;
    maxInactiveMinutes = 30;
    constructor(public auth: AuthService, private router: Router) {
        auth.handleAuthentication();
        auth.scheduleRenewal();
    }

    ngOnInit() {
        // creates interval, that will check every minute if activity was done
        this.interval = setInterval(
            () => {
                // if user made a action and is authenticated proceed towards redirect
                if (!this.action && this.auth.isAuthenticated()) {
                    // if inactive minutes exceeds maximum allowed inactivity proceed with login redirect
                    if (this.inactiveMinutes++ > this.maxInactiveMinutes) {
                        this.auth.logout();
                        this.router.navigate(['/login']);
                    }
                } else {
                    // nulify the inacitivity time
                    this.action = false;
                    this.inactiveMinutes = 0;
                }
            }
            , 60000);

    }

    ngOnDestroy() {
        clearInterval(this.interval);
    }

}
