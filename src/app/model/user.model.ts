export interface IUser {
    id: String;
    userName: string;
    organizationId: string;
    firstName: string;
    lastName?: string;
    age: number;
    email: string;
    birthDate?: Date;
    created?: Date;
    modified?: Date;
}
