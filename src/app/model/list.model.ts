export interface IList<T> {
    data: Array<T>,
    totalCount?: number,
}
