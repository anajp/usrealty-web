import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { CallbackComponent } from './components/callback/callback.component';
import { UsersComponent } from "./components/users/list/users.component";
import { AuthGuardService as AuthGuard } from './shared/auth/auth-guard.service';
import { LoginComponent } from './components/login/login.component';

export const routes: Routes = [
    {
        path: '',
        component: UsersComponent,
        pathMatch: 'full',
        canActivate: [AuthGuard]
    },
    { path: 'login', component: LoginComponent },
    {
        path: 'callback',
        component: CallbackComponent
    },
    {
        path: '**', redirectTo: 'home', pathMatch: 'full'
    }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, { useHash: false });
