import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../../shared/auth/auth.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html'
})
export class LoginComponent {
    loading: boolean;
    hide: boolean;

    constructor(public auth: AuthService) {
        this.loading = false;
        this.hide = true;
    }

    login(user, password) {
        this.loading = true;
        this.auth.login(user, password)
            .catch(reason => {
                this.loading = false;
            });
    }
}


