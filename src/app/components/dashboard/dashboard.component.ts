import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/auth/auth.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    providers: [AuthService]
})
export class DashboardComponent implements OnInit {

    profile: any;

    constructor(public auth: AuthService) {
    }

    ngOnInit () {
        this.auth.loggedIn$.subscribe((isLoggedIn) => {
            if (isLoggedIn) {
                if (this.auth.userProfile) {
                    this.profile = this.auth.userProfile;
                } else {
                    this.auth.getProfile((err, profile) => {
                        this.profile = profile;
                    });
                }
            }
        });
    }
}
