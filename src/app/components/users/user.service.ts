import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';

import { IUser } from '../../model/user.model';
import { IList } from '../../model/list.model';

import { environment } from './../../../environments/environment';
import { HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { HttpResponse } from '@angular/common/http';

@Injectable()
export class UserService {

    /**
     * @description Function to deal with response errors.
     * @param error
     * @returns {any}
     */
    private static handleError (error: any): Promise<any> {
        console.log('user.service: An error occurred', error.statusText);
        return Promise.reject(error.message || error);
    }

    /**
     * @description Function to deal with response errors.
     * @returns {IList<IUser>}
     * @param data
     */
    private static extract (response: HttpResponse<Object>): IList<IUser> {
        return {
            data: response.body as Array<IUser>,
            totalCount: Array.isArray(response.body) ? response.body.length : (!!response.body ? 1 : 0)
        };
    }

    constructor(private http: HttpClient) { }

    getUsers (): Promise<IList<IUser>> {
        return this.http.get<IUser>(`${environment.serverUrl}/user/all`, { observe: 'response', responseType: 'json' })
            .toPromise()
            .then(res => res)
            .then(UserService.extract)
            .catch(UserService.handleError);
    }

    getUserById (id: string): Promise<IUser> {
        return this.http.get(`${environment.serverUrl}/user/${id}`)
            .toPromise()
            .then(UserService.extract)
            .catch(UserService.handleError);
    }
}
