import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UserService } from './../user.service';
import { IUser } from '../../../model/user.model';
import { MatSnackBar } from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import { PageEvent } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html'
})
export class UsersComponent implements OnInit {
    displayedColumns = ['userName', 'organizationId', 'firstName', 'lastName', 'age', 'email', 'birthDate'];
    userDatabase = null;
    length: number;
    dataSource: UserDataSource | null;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('filter') filter: ElementRef;

    constructor(private userService: UserService, public snackBar: MatSnackBar) {
        this.userDatabase = new UserDatabase(userService);
    }

    handleRowClick (row: IUser) {
        this.snackBar.open('' + row.email + '', 'Click', {
            duration: 2000,
        });
    }

    ngOnInit () {
        this.dataSource = new UserDataSource(this.userDatabase, this.paginator);

        Observable.fromEvent(this.filter.nativeElement, 'keyup')
            .debounceTime(150)
            .distinctUntilChanged()
            .subscribe(() => {
                if (!this.dataSource) { return; }
                this.dataSource.filter = this.filter.nativeElement.value;
            });
    }
}

export class UserDatabase {
    dataChange: BehaviorSubject<IUser[]> = new BehaviorSubject<IUser[]>([]);
    loading = true;
    _count: number;
    get data (): IUser[] { return this.dataChange.value; }
    get count (): number { return this._count; }

    constructor(private userService: UserService) {
        this.getUsers();
    }

    getUsers () {
        this.userService.getUsers()
            .then(users => {
                this._count = users.totalCount;
                this.loading = false;
                this.dataChange.next(users.data);
            }).catch(() => {
                this.loading = false;
            });
    }
}

export class UserDataSource extends DataSource<any> {
    _filterChange = new BehaviorSubject('');
    get filter (): string { return this._filterChange.value; }
    set filter (filter: string) { this._filterChange.next(filter); }

    constructor(private _userDatabase: UserDatabase, private _paginator: MatPaginator) {
        super();
    }

    connect (): Observable<IUser[]> {
        const displayDataChanges = [
            this._userDatabase.dataChange,
            this._paginator.page,
            this._filterChange
        ];

        return Observable.merge(...displayDataChanges).map(() => {
            const data = this._userDatabase.data.slice();

            const startIndex = this._paginator.pageIndex * this._paginator.pageSize;

            return data.splice(startIndex, this._paginator.pageSize).filter((item: IUser) => {
                let searchStr = (item.userName).toLowerCase();
                return searchStr.indexOf(this.filter.toLowerCase()) != -1;
            });
        });
    }

    disconnect () { }
}
