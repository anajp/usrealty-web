export const environment = {
    production: true,
    serverUrl: 'http://localhost:4900/api/v1',

    clientID: 'HNst0ilAdlNDrD5ZXvnJ33ae4lY4ia16',
    domain: 'sdcauth.auth0.com',
    callbackURL: 'http://localhost:4200/callback',
    audience: 'https://sdcauth.auth0.com/userinfo'
};
